const express = require('express')
const router = express.Router()

const endpoints = require('../endpoints')
const bodyParser = require('body-parser')

router.use(bodyParser.json())

// Default
router.post('/', (req, res, next) => {
    res.send('Try again!')
})

// GetReport
router.post('/GetReport', (req, res, next) => {
    endpoints.GetReport(
        res,
        req.body.ReportId,
        next
    );
})

// RequestReport
router.post('/RequestReport', (req, res, next) => {
    endpoints.RequestReport(
        res,
        req.body.ReportType,
        req.body.StartDate,
        req.body.EndDate,
        req.body.ReportOptions,
        req.body.MarketplaceIdList,
        next
    );
})

// ListMatchingProducts
router.post('/ListMatchingProducts', (req, res, next) => {
    endpoints.ListMatchingProducts(
        res,
        req.body.MarketplaceId,
        req.body.Query,
        req.body.QueryContextId,
        next
    );
})

// GetCompetitivePricingForSKU
router.post('/GetCompetitivePricingForSKU', (req, res, next) => {
    endpoints.GetCompetitivePricingForSKU(
        res,
        req.body.MarketplaceId,
        req.body.SellerSKUList,
        next
    )
})

// GetCompetitivePricingForASIN
router.post('/GetCompetitivePricingForASIN', (req, res, next) => {
    endpoints.GetCompetitivePricingForASIN(
        res,
        req.body.MarketplaceId,
        req.body.ASINList,
        next
    )
})

// GetLowestOfferListingsForASIN
router.post('/GetLowestOfferListingsForASIN', (req, res, next) => {
    endpoints.GetLowestOfferListingsForASIN(
        res,
        req.body.MarketplaceId,
        req.body.ASINList,
        req.body.ItemCondition, // Any, New, Used, Collectible, Refurbished, Club (optional)
        next
    )
})

// GetLowestOfferListingsForSKU
router.post('/GetLowestOfferListingsForSKU', (req, res, next) => {
    endpoints.GetLowestOfferListingsForSKU(
        res,
        req.body.MarketplaceId,
        req.body.SellerSKUList,
        req.body.ItemCondition, // Any, New, Used, Collectible, Refurbished, Club (optional)
        next
    )
})

// GetLowestPricedOffersForSKU
router.post('/GetLowestPricedOffersForSKU', (req, res, next) => {
    endpoints.GetLowestPricedOffersForSKU(
        res,
        req.body.MarketplaceId,
        req.body.SellerSKU, //
        req.body.ItemCondition, // Any, New, Used, Collectible, Refurbished, Club (optional)
        next
    )
})

// GetLowestPricedOffersForASIN
router.post('/GetLowestPricedOffersForASIN', (req, res, next) => {
    endpoints.GetLowestPricedOffersForASIN(
        res,
        req.body.MarketplaceId,
        req.body.ASIN, //
        req.body.ItemCondition, // Any, New, Used, Collectible, Refurbished, Club (optional)
        next
    )
})

// GetMatchingProduct
router.post('/GetMatchingProduct', (req, res, next) => {
    endpoints.GetMatchingProduct(
        res,
        req.body.MarketplaceId,
        req.body.ASINList,
        next
    )
})

// GetMatchingProductForId
router.post('/GetMatchingProductForId', (req, res, next) => {
    endpoints.GetMatchingProductForId(
        res,
        req.body.MarketplaceId,
        req.body.IdType, // ASIN, GCID, SellerSKU, UPC, EAN, ISBN, and JAN
        req.body.IdList,
        next
    )
})

// GetMyFeesEstimate
router.post('/GetMyFeesEstimate', (req, res, next) => {
    endpoints.GetMyFeesEstimate(
        res,
        req.body.FeesEstimateRequestList,
        next
    )
})

// GetMyPriceForSKU
router.post('/GetMyPriceForSKU', (req, res, next) => {
    endpoints.GetMyPriceForSKU(
        res,
        req.body.MarketplaceId,
        req.body.SellerSKUList, //
        req.body.ItemCondition, // Any, New, Used, Collectible, Refurbished, Club (optional)
        next
    )
})

// GetMyPriceForASIN
router.post('/GetMyPriceForASIN', (req, res, next) => {
    endpoints.GetMyPriceForASIN(
        res,
        req.body.MarketplaceId,
        req.body.ASINList, //
        req.body.ItemCondition, // Any, New, Used, Collectible, Refurbished, Club (optional)
        next
    )
})

// GetProductCategoriesForASIN
router.post('/GetProductCategoriesForASIN', (req, res, next) => {
    endpoints.GetProductCategoriesForASIN(
        res,
        req.body.MarketplaceId,
        req.body.ASIN,
        next
    )
})

// GetProductCategoriesForSKU
router.post('/GetProductCategoriesForSKU', (req, res, next) => {
    endpoints.GetProductCategoriesForSKU(
        res,
        req.body.MarketplaceId,
        req.body.SellerSKU,
        next
    )
})

// GetServiceStatus
router.post('/GetServiceStatus', (req, res, next) => {
    endpoints.GetServiceStatus(
        res,
        next
    )
})

module.exports = router;