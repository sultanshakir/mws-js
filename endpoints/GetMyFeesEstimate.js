const accessKey = process.env.AWS_ACCESS_KEY_ID;
const accessSecret = process.env.AWS_SECRET_ACCESS_KEY;

let amazonMws = require('amazon-mws')(accessKey, accessSecret);

function GetMyFeesEstimate(res, FeesEstimateRequestList, next) {

    var data = {
        'Version': '2011-10-01',
        'Action': 'GetMyFeesEstimate',
        'SellerId': 'A34XJ5KJR2Z8DG',
        'MWSAuthToken': 'amzn.mws.94ad718c4b091ee15f029b0881902ac8'
    };
    var index = 1;
    var ferl = 'FeesEstimateRequestList.FeesEstimateRequest.';
    var ferlarr = FeesEstimateRequestList;
    for (var i in ferlarr) {
        // data['IdList.Id.' + index] = IdList[i];
        
        data[ferl + index + '.MarketplaceId'] = ferlarr[i]['MarketplaceId'];
        data[ferl + index + '.IdType'] = ferlarr[i]['IdType'];
        data[ferl + index + '.IdValue'] = ferlarr[i]['IdValue']; // ASIN, SellerSKU
        data[ferl + index + '.IsAmazonFulfilled'] = ferlarr[i]['IsAmazonFulfilled'];
        data[ferl + index + '.Identifier'] = ferlarr[i]['Identifier'];
        data[ferl + index + '.PriceToEstimateFees.ListingPrice.CurrencyCode'] = ferlarr[i]['PriceToEstimateFees.ListingPrice.CurrencyCode'];
        data[ferl + index + '.PriceToEstimateFees.ListingPrice.Amount'] = ferlarr[i]['PriceToEstimateFees.ListingPrice.Amount'];
        data[ferl + index + '.PriceToEstimateFees.Shipping.CurrencyCode'] = ferlarr[i]['PriceToEstimateFees.Shipping.CurrencyCode'];
        data[ferl + index + '.PriceToEstimateFees.Shipping.Amount'] = ferlarr[i]['PriceToEstimateFees.Shipping.Amount'];
        data[ferl + index + '.PriceToEstimateFees.Points.PointsNumber'] = ferlarr[i]['PriceToEstimateFees.Points.PointsNumber'];
        
        index++;
    }

    amazonMws.products.search(data, function(error, response) {
        if (error) {
            next(error);
            return;
        }
        //console.log('response ', JSON.stringify(response));
        res.send(response);
    });
};

module.exports = GetMyFeesEstimate;
