module.exports.ListMatchingProducts = require('./ListMatchingProducts')
module.exports.GetCompetitivePricingForSKU = require('./GetCompetitivePricingForSKU') // Returns the current competitive price of a product, based on SellerSKU.
module.exports.GetCompetitivePricingForASIN = require('./GetCompetitivePricingForASIN') // Returns the current competitive price of a product, based on ASIN.
module.exports.GetLowestOfferListingsForASIN = require('./GetLowestOfferListingsForASIN')
module.exports.GetLowestOfferListingsForSKU = require('./GetLowestOfferListingsForSKU')
module.exports.GetLowestPricedOffersForSKU = require('./GetLowestPricedOffersForSKU')
module.exports.GetLowestPricedOffersForASIN = require('./GetLowestPricedOffersForASIN')
module.exports.GetMatchingProduct = require('./GetMatchingProduct')
module.exports.GetMatchingProductForId = require('./GetMatchingProductForId')
module.exports.GetMyFeesEstimate = require('./GetMyFeesEstimate')
module.exports.GetMyPriceForSKU = require('./GetMyPriceForSKU')
module.exports.GetMyPriceForASIN = require('./GetMyPriceForASIN')
module.exports.GetProductCategoriesForSKU = require('./GetProductCategoriesForSKU')
module.exports.GetProductCategoriesForASIN = require('./GetProductCategoriesForASIN')
module.exports.GetServiceStatus = require('./GetServiceStatus')

// Reports
module.exports.GetReport = require('./GetReport')
module.exports.RequestReport = require('./RequestReport')
