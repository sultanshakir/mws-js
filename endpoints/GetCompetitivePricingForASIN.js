const accessKey = process.env.AWS_ACCESS_KEY_ID;
const accessSecret = process.env.AWS_SECRET_ACCESS_KEY;

let amazonMws = require('amazon-mws')(accessKey, accessSecret);

function GetCompetitivePricingForASIN (res, MarketplaceId, ASINList, next) {

    var data = {
        'Version': '2011-10-01',
        'Action': 'GetCompetitivePricingForASIN',
        'SellerId': 'A34XJ5KJR2Z8DG',
        'MWSAuthToken': 'amzn.mws.94ad718c4b091ee15f029b0881902ac8',
        'MarketplaceId': MarketplaceId
    };
    var index = 1;
    for (var i in ASINList) {
        data['ASINList.ASIN.' + index] = ASINList[i];
        index++;
    }
    amazonMws.products.search(data, function (error, response) {
        if (error) {
            next(error);
            return;
        }
        res.send(response);
    });
};

module.exports = GetCompetitivePricingForASIN;