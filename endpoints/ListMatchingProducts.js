const accessKey = process.env.AWS_ACCESS_KEY_ID;
const accessSecret = process.env.AWS_SECRET_ACCESS_KEY;

let amazonMws = require('amazon-mws')(accessKey, accessSecret);

function ListMatchingProducts (res, MarketplaceId, Query, QueryContextId, next) {
     
    amazonMws.products.search({
        'Version': '2011-10-01',
        'Action': 'ListMatchingProducts',
        'SellerId': 'A34XJ5KJR2Z8DG',
        'MWSAuthToken': 'amzn.mws.94ad718c4b091ee15f029b0881902ac8',
        'MarketplaceId': MarketplaceId,
        'Query': Query,
        'QueryContextId': QueryContextId
    }, function (error, response) {
        if (error) {
            next(error);
            return;
        }
        res.send(response);
    });
    
};

module.exports = ListMatchingProducts;