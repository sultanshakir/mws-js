const accessKey = process.env.AWS_ACCESS_KEY_ID;
const accessSecret = process.env.AWS_SECRET_ACCESS_KEY;

let amazonMws = require('amazon-mws')(accessKey, accessSecret);

function RequestReport (res, ReportType, StartDate, EndDate, ReportOptions, MarketplaceIdList, next) {

    var data = {
        'Version': '2009-01-01',
        'Action': 'RequestReport',
        'ReportType': ReportType,
        'StartDate': StartDate || new Date(),
        'EndDate': EndDate || new Date(),
        'ReportOptions': ReportOptions,
        'SellerId': 'A34XJ5KJR2Z8DG',
        'MWSAuthToken': 'amzn.mws.94ad718c4b091ee15f029b0881902ac8'
    };
    var index = 1;
    for (var i in MarketplaceIdList) {
        data['MarketplaceIdList.Id.' + index] = MarketplaceIdList[i];
        index++;
    }
    amazonMws.reports.search(data, function (error, response) {
        if (error) {
            next(error);
            return;
        }
        //console.log('response ', JSON.stringify(response));
        res.send(response);
    });
};

module.exports = RequestReport;