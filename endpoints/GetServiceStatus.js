const accessKey = process.env.AWS_ACCESS_KEY_ID;
const accessSecret = process.env.AWS_SECRET_ACCESS_KEY;

let amazonMws = require('amazon-mws')(accessKey, accessSecret);

function GetServiceStatus(res, next) {

    var data = {
        'Action': 'GetServiceStatus',
        'MWSAuthToken': 'amzn.mws.94ad718c4b091ee15f029b0881902ac8',
        'SellerId': 'A34XJ5KJR2Z8DG',
        'Version': '2011-10-01'
    };
    amazonMws.products.search(data, function(error, response) {
        if (error) {
            next(error);
            return;
        }
        //console.log('response ', JSON.stringify(response));
        res.send(response);
    });
};

module.exports = GetServiceStatus;
