'use strict';

const express = require('express')
const basicAuth = require('express-basic-auth')
const routes = require('./actions/routes')
const cors = require('cors')
const winston = require('winston')

const app = express()

app.disable('x-powered-by')

const logger = winston.createLogger({
    transports: [
        new winston.transports.Console(),
        new winston.transports.File({ filename: 'info/dalog.log' })
    ]
});

const mdware = [
    cors({origin: "http://localhost:8081"}),
    express.static('public'),
    basicAuth({ users: { 'admin': 'sunnah' } }),
    routes,
    (err, req, res, next) => {
        console.log('error products', err);
        logger.log({
            level: 'error',
            "ErrorMessage": err,
            "RequestHeaders":req.headers,
            "RequestBody": req.body,
            "PathRequested": req.headers["x-forwarded-proto"] + '://' + req.hostname + req.originalUrl,
            "Time": Date()
        }),
        err.RequestHeaders = req.headers,
        err.RequestBody = req.body,
        res.json({Error: err})
    }
]

app.use('/api', mdware)

app.listen(process.env.PORT, () => console.log('Example app listening on port '+ process.env.PORT +'!'))